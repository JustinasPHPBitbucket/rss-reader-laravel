# README #


### What is this repository for? ###

* Rss Reader
* Version 1.0.0
* Laravel version 5.6



### How do I get set up? ###

* Download project
* Configure you IDE to remote ubuntu server 16.04.4 LTS
* Upload this project to your remote server 
* Make sure your server meets the following requirements:
* PHP >= 7.1.3
* OpenSSL PHP Extension
* PDO PHP Extension
* Mbstring PHP Extension
* Tokenizer PHP Extension
* XML PHP Extension
* Ctype PHP Extension
* JSON PHP Extension
* https://laravel.com/docs/5.6


* Change file .env.example to .env and edit it for your needs
* Connect your remote server via SSH Client
* Go to project folder
* From the root project folder run these commands in command line:
* composer install
* npm install
* php artisan key:generate
* npm run watch


### Rss Reader guidelines ###

* Register new user in command line:
* php artisan user:create :name :email :password
* Launch browser and login
* Press EDIT CATEGORIES and create new category
* Press ADD NEW FEED and in input field enter valid rss source url
* f.ex. http://feeds.bbci.co.uk/news/business/rss.xml
* Then press Follow button
* Later you can update feeds in command line
* php artisan feed:update
* That's all!:)

### You can contact me ###

* My name is Justinas
* Email: justinas.straukas@gmail.com
