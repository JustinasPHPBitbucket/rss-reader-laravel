<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function channels()
    {
        return $this->hasMany(Channel::class);
    }
    public function items()
    {
        return $this->hasManyThrough(Item::class, Channel::class);
    }
}
