<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Channel;
use App\Item;

class UpdateFeeds extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'feed:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update all feeds by their url addresses';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $channels = Channel::all();
        $bar = $this->output->createProgressBar(count($channels));

        foreach ($channels as $channel) {
            $this->updateFeed($channel->channel_xml_url, $channel->id);
            $bar->advance();
        }

        $bar->finish();
        $this->info("\nAll feeds have been updated!");
    }

    /**
     * @param $url
     * @param $channel_id
     */
    private function updateFeed($url, $channel_id)
    {

        $loader = resolve('LoadFeed');
        $source = $loader->source($url);
        $feed = Channel::find($channel_id);
        $feed->channel_title = $source->channel_title;
        $feed->channel_description = $source->channel_description;
        $feed->channel_link = $source->channel_link;
        $feed->channel_lastBuildDate = date("Y-m-d h:i:s", strtotime($source->channel_lastBuildDate));
        $feed->save();
        $feed->items()->delete();

        foreach ($source->items as $item) {
            $feed->items()->saveMany([
                new Item([
                    'item_title' => $item->item_title,
                    'item_description' => $item->item_description,
                    'item_link' => $item->item_link,
                    'item_pubDate' => date("Y-m-d h:i:s", strtotime($item->item_pubDate)),
                    'item_mediaThumbnail' => $item->item_mediaThumbnail,
                    'item_mediaContent' => $item->item_mediaContent
                ])
            ]);
        }
    }
}
