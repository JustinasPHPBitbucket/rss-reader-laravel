<?php

namespace App\Console\Commands;

use Validator;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;
use App\User;

class UserCreateCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:create 
    {username : required user name},
    {email : required user email}, 
    {password : required password}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new user ';


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $email = Validator::make($this->arguments(), [
            'email' => 'string|email|max:255|unique:users,email'
        ]);

        if ($email->fails()) {
            $errors = $email->errors();
            foreach ($errors->get('email') as $message) {
                $this->error($message);
            }
            return;
        }
        $username = $this->argument('username');
        $email = $this->argument('email');
        $password = $this->argument('password');
        $repeated_password = $this->ask('Please, repeat the password: ');
        if ($password !== $repeated_password) {
            $this->error('You have entered different password, please create a new user again.');
            return;
        }
        $user = new User;
        $user->name = $username;
        $user->email = $email;
        $user->password = \Hash::make($password);
        $user->save();
        $this->info("\nYou have successfully created a new user!");
        $this->line("User name - $user->name\nEmail - $user->email \nPassword - {$this->argument('password')} ");
    }

}
