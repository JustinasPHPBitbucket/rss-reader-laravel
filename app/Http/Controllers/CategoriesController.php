<?php

namespace App\Http\Controllers;

use App\Http\Requests\RequestCategory;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Category;
use App\User;

class CategoriesController extends Controller
{
    /**
     * @param RequestCategory $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(RequestCategory $request)
    {
        $cat_names = User::find(Auth::id())->categories()->pluck('category_name')->toArray();

        if (in_array($request->category_name, $cat_names)) {
            session()->flash('alert-danger', 'Category <strong> ' . $request->category_name . ' </strong>already exists! Please, enter a new category name');
            return redirect()->back();
        }

        $category = new Category;
        $category->category_name = $request->category_name;
        $category->user_id = $request->user_id;
        $category->save();
        session()->flash('alert-success', 'Category <strong>' . $category->category_name . ' </strong>was successfully created!');
        return redirect()->back();

    }

    /**
     * @param RequestCategory $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(RequestCategory $request)
    {
        $category = Category::find($request->category_id);
        $category->category_name = $request->category_name;
        $category->update();
        session()->flash('alert-success', 'Category was successfully renamed to - <strong>' . $category->category_name . ' </strong>!');
        return redirect()->route('admin');
    }


    public function destroy(Request $request)
    {
        $category = Category::find($request->category_id);
        $category->items()->delete();
        $category->channels()->delete();
        $category->delete();
        session()->flash('alert-success', 'Category <strong>' . $category->category_name . ' </strong>was successfully deleted!');
        return redirect()->route('admin');
    }
}
