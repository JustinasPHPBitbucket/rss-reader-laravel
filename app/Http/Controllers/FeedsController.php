<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Channel;
use App\Item;
use App\Category;

class FeedsController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $loader = resolve('LoadFeed');
        $source = $loader->source($request->url);
        $current_category = Category::find($request->category_id);
        $channels_sources = $current_category->channels()->pluck('channel_xml_url')->toArray();

        if (in_array($source->channel_xml_url, $channels_sources)) {
            session()->flash('alert-danger', 'Feed <strong> ' . $source->channel_title . ' </strong>already exists in category <strong>' . $current_category->category_name . '</strong>!');
            return redirect()->back();
        }

        $feed = new Channel;
        $feed->category_id = $request->category_id;
        $feed->channel_xml_url = $source->channel_xml_url;
        $feed->channel_title = $source->channel_title;
        $feed->channel_description = $source->channel_description;
        $feed->channel_link = $source->channel_link;
        $feed->channel_lastBuildDate = date("Y-m-d h:i:s", strtotime($source->channel_lastBuildDate));
        $feed->save();

        $currentfeed = Channel::latest()->first();

        foreach ($source->items as $item) {
            $currentfeed->items()->saveMany([
                new Item([
                    'item_title' => $item->item_title,
                    'item_description' => $item->item_description,
                    'item_link' => $item->item_link,
                    'item_pubDate' => date("Y-m-d h:i:s", strtotime($item->item_pubDate)),
                    'item_mediaThumbnail' => $item->item_mediaThumbnail,
                    'item_mediaContent' => $item->item_mediaContent
                ])
            ]);
        }
        session()->flash('alert-success', 'Feed <strong> ' . $feed->channel_title . ' </strong>was successfully added to category <strong>' . $currentfeed->category->category_name . '</strong>!');
        return redirect('/admin/feeds/' . $currentfeed->id);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function deleteFeed(Request $request)
    {
        $feed = Channel::find($request->delete_feed_id);
        $current_category_id = $feed->category->id;
        $current_category_name = $feed->category->category_name;
        $feed->items()->delete();
        $feed->delete();
        session()->flash('alert-success', 'Feed <strong>' . $feed->channel_title . ' </strong > was successfully removed from category <strong>' . $current_category_name . '</strong> !');
        return redirect('/admin/categories/' . $current_category_id);
    }
}
