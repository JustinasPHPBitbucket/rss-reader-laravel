<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use Illuminate\Support\Facades\Auth;
use App\User;

class PagesController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $user_id = Auth::id();
        $user = User::find($user_id);
        $feeds = $user->channels()->orderBy('channel_title')->get();
        $categories = $user->categories;
        $heading = "All feeds";
        return view('admin.pages.admin_home_page', compact('categories', 'feeds', 'user_id', 'heading'));

    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function addContent()
    {
        $user_id = Auth::id();
        $categories = Category::all()->where('user_id', Auth::id());

        return view('admin.pages.addcontent', compact('categories', 'user_id'));
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function liveSearch(Request $request)
    {
        if ($request->ajax()) {
            $xmlDoc = new \DOMDocument();
            $xmlDoc->load($request->url);
            $items = $xmlDoc->getElementsByTagName('item');
            return \View::make('admin.templates.feeds.livesearch')->with('items', $items);
        }
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function getChannelTitleAndAllItems(Request $request)
    {
        $loader = resolve('LoadFeed');
        $feed = $loader->source($request->url);
        return \View::make('admin.templates.feeds.channelandfeeds', compact('feed'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function filterByCategories(Request $request, $id)
    {
        $user_id = Auth::id();
        $user = User::find($user_id);
        $categories = $user->categories;

        if (!$user->categories()->find($id)) {
            return redirect()->back();
        }

        $feeds = $user->channels()->where('category_id', $id)->orderBy('channel_title')->get();

        if ($request->ajax()) {
            return $feeds;
        }

        $heading = 'Feeds in category - ' . $user->categories()->find($id)->category_name;
        return view('admin.pages.admin_home_page', compact('categories', 'feeds', 'user_id', 'heading'));
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function currentFeed($id)
    {
        $user_id = Auth::id();
        $user = User::find($user_id);

        if (!$user->channels()->find($id)) {
            return redirect()->back();
        }
        $feed = $user->channels()->find($id);
        $categories = $user->categories;
        return view('admin.pages.view_current_feed', compact('categories', 'feed', 'user_id'));
    }

}
