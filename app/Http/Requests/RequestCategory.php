<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class  RequestCategory extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            'category_name' => 'required|max:50|regex:/^[\pL\s0-9-_]+$/u'
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'category_name.regex' => 'Only letters, spaces, underscores allowed.'
        ];
    }
}
