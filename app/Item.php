<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $guarded = ['id'];
    public function channel()
    {
        return $this->belongsTo(Chennel::class);
    }
}
