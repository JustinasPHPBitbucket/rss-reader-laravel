<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\LoadFeed;

class FeedsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {

    }

    /**
 * Register services.
 *
 * @return void
 */
    public function register()
    {
        $this->app->bind('LoadFeed', function() {
          return  new LoadFeed;
        });
    }
}
