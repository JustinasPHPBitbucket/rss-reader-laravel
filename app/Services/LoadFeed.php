<?php

namespace App\Services;

class LoadFeed
{
    /**
     * @param $url
     * @return __anonymous@163
     */
    public function source($url)
    {
        $xmlDoc = new \DOMDocument();

        $feed = new class
        {
            public $category_id;
            public $channel_title;
            public $channel_link;
            public $channel_xml_url;
            public $channel_description;
            public $channel_lastBuildDate;
            public $items = [];
        };
        $xmlDoc->load($url);

        $channel = $xmlDoc->getElementsByTagName('channel')->item(0);
        $feed->channel_xml_url = $url;
        $feed->channel_title = $channel->getElementsByTagName('title')->item(0)->childNodes->item(0)->nodeValue;
        $feed->channel_link = parse_url($channel->getElementsByTagName('link')->item(0)->childNodes->item(0)->nodeValue, PHP_URL_HOST);
        $feed->channel_description = $channel->getElementsByTagName('description')->item(0)->childNodes->item(0)->nodeValue;
        if ($channel->getElementsByTagName('lastBuildDate')->length !== 0) {
            $feed->channel_lastBuildDate = $channel->getElementsByTagName('lastBuildDate')->item(0)->childNodes->item(0)->nodeValue;
        } else {
            $feed->channel_lastBuildDate = null;
        }
        //get all "<item> elements of <channel>"
        $items = $xmlDoc->getElementsByTagName('item');


        foreach ($items as $item) {
            $itm = new class
            {
                public $channel_id;
                public $item_title;
                public $item_description;
                public $item_link;
                public $item_pubDate;
                public $item_mediaThumbnail;
                public $item_mediaContent;
            };


            $itm->item_title = $item->getElementsByTagName('title')->item(0)->childNodes->item(0)->nodeValue;

            if ($item->getElementsByTagName('description')->length !== 0) {
                $itm->item_description = $item->getElementsByTagName('description')->item(0)->childNodes->item(0)->nodeValue;
            } else {
                $itm->item_description = null;
            }
            if ($item->getElementsByTagName('link')->length !== 0) {
                $itm->item_link = $item->getElementsByTagName('link')->item(0)->childNodes->item(0)->nodeValue;
            } else {
                $itm->item_link = null;
            }
            $itm->item_pubDate = $item->getElementsByTagName('pubDate')->item(0)->childNodes->item(0)->nodeValue;

            if ($item->getElementsByTagName('thumbnail')->length !== 0) {
                $itm->item_mediaThumbnail = $item->getElementsByTagName('thumbnail')->item(0)->getAttribute('url');
            } else {
                $itm->item_mediaThumbnail = null;
            }
            if ($item->getElementsByTagName('content')->length !== 0) {
                $itm->item_mediaContent = $item->getElementsByTagName('content')->item(0)->getAttribute('url');
            } else {
                $itm->item_mediaContent = null;
            }
            $feed->items[] = $itm;
        }
        return $feed;
    }
}