<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChannelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    { Schema::create('channels', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('category_id');
        $table->text('channel_xml_url');
        $table->string('channel_title');
        $table->text('channel_description');
        $table->string('channel_link');
        $table->timestamp('channel_lastBuildDate')->nullable();
        $table->timestamps();
    });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('channels');
    }
}
