<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('channel_id');
            $table->string('item_title');
            $table->text('item_description')->nullable();
            $table->text('item_link')->nullable();
            $table->timestamp('item_pubDate')->nullable();
            $table->text('item_mediaThumbnail')->nullable();
            $table->text('item_mediaContent')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
