(function ($) {
    $(function () {
        let input_text;
        let category_id = null;
        let categoriesModal = $("#categoriesModal");
        let selectCategoryInput = $("#selectCategoryInput");
        let btn_cat_rename = $("#cat_rename");
        let btn_cat_delete = $("#cat_delete");
        let btn_cat_cancel = $("#cat_cancel");
        let btn_cat_rename_save = $("#cat_rename_save");
        let drop_down = $("#edit_category_drop_down");
        // Focus input
        $(selectCategoryInput).focus(function () {
            if ($(this).prop('readonly') === true) {
                $(drop_down).removeClass("invisible");

            }
        });

        // Input change event only if editable
        $(selectCategoryInput).bind('change paste keyup', function () {

            if ($(this).prop('readonly') !== true) {
                if (input_text !== $(this).val()) {
                    $(btn_cat_rename).addClass("d-none");
                    $(btn_cat_rename_save).removeClass("d-none");

                } else {

                    $(btn_cat_rename).removeClass("d-none");
                    $(btn_cat_rename_save).addClass("d-none");
                }

            }
        });
        // Click list item - hide drop down, select category, enable Rename button
        $(drop_down).find('.list-group-item').click(function () {
            input_text = $(this).find(".cat_title").text();
            category_id = $(this).find('input[type="hidden"]').val();
            $(drop_down).addClass("invisible");
            $(selectCategoryInput).val(input_text);
            $(btn_cat_delete).removeClass('d-none');
            $(btn_cat_rename).removeClass("disabled");
            console.log('Category id - ' + category_id);
            console.log('Selected category name - ' + input_text);
            initializePopoverForCategoryDelete();
            initializePopoverForCategoryRename();
        });
        // rename Category
        $(btn_cat_rename).click(function () {
            if ($(selectCategoryInput).val() !== '') {
                $(btn_cat_cancel).removeClass("d-none");
                $(selectCategoryInput).prop('readonly', false).focus();
            }

        });

        // Cancel rename category
        $(btn_cat_cancel).click(function () {
            $(selectCategoryInput).val(input_text);
            $(selectCategoryInput).prop('readonly', true);
            $(btn_cat_rename).removeClass("d-none");
            $(btn_cat_rename_save).addClass("d-none");
            $(this).addClass('d-none');


        });

        // Hide dropdown
        $(categoriesModal).find(".modal-header, .modal-footer, p").click(function () {
            $(drop_down).addClass("invisible");

        });
        // Reset modal
        $(categoriesModal).on('hide.bs.modal', function () {
            input_text = '';
            category_id = null;
            $(selectCategoryInput).val(input_text);
            $("#category-name").val('');
            $(selectCategoryInput).prop('readonly', true);
            $(btn_cat_rename).addClass("disabled");
            $(btn_cat_rename).removeClass("d-none");
            $(btn_cat_delete).addClass('d-none');
            $(btn_cat_cancel).addClass('d-none');
            $(btn_cat_rename_save).addClass("d-none");

            $("#collapse_new_category").collapse('hide');

        });

        function initializePopoverForCategoryDelete() {
            $(btn_cat_delete).popover({
                container: 'body',
                trigger: "focus",
                html: true,
                template: '<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-header"></h3><div class="popover-body"></div></div>',
                title: "Ar you sure you want to delete this category and <strong>all feeds</strong> under it?",
                content: function () {
                    return `
    <div class="form-group">
        <form action="/admin/delete-category" method="post" class="d-flex justify-content-around align-items-center">
            <input type="hidden" name="category_id" value="${category_id}">
            <input type="hidden" name="_token" value="${$('meta[name="csrf-token"]').attr('content')}">
            <button type="submit" class="btn btn-outline-danger col-6 mx-1">Yes</button>
            <button type="button" class="btn btn-outline-secondary col-6 mx-1">No</button>
        </form>
    </div>`;
                },
            });
        } // initializePopoverForCategoryDelete

        function initializePopoverForCategoryRename() {
            $(btn_cat_rename_save).popover({
                container: 'body',
                html: true,
                template: '<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-header"></h3><div class="popover-body"></div></div>',
                title: "Ar you sure you want to <strong>rename</strong> this category?",
                content: function () {
                    return `<div class="form-group">
        <form action="/admin/rename-category" method="post" class="d-flex justify-content-around align-items-center">
            <input type="hidden" name="category_id" value="${category_id}">
            <input type="hidden" name="category_name" value="${$(selectCategoryInput).val()}">
            <input type="hidden" name="_token" value="${$('meta[name="csrf-token"]').attr('content')}">
            
            <button type="submit" class="btn btn-outline-success col-6 mx-1">Yes</button>
            <button type="button" class="btn btn-outline-secondary col-6 mx-1">No</button>
        </form>
    </div>`;
                },
            });
        } // initializePopoverForCategoryRename

    }); // ready
})(jQuery);

