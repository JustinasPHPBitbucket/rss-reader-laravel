(function ($) {
    $(function () {
        let live_search_input = $("#live_search_url_input");
        let live_search_drop_down = $("#live_search_drop_down");
        let all_feeds = $("#all_feeds");
        $(live_search_input).focus();

        let urlInputValueBeforeEvent = "";

        $(live_search_input).on("paste change keyup", function () {

            let urlInput = ($(this).val()).trim();

            console.log("event");

            /* if url is the same after event - do not call any action */

            if (urlInputValueBeforeEvent !== urlInput) {


                if (urlInput === "") {
                    /* if empty input - clear autosuggestion field */
                    $(all_feeds).empty();
                    return;
                }

                urlInputValueBeforeEvent = urlInput;
                requestsRouter(urlInput);

            } else {
                console.log("Input value has not not changed: " + urlInputValueBeforeEvent);
                console.log("No action is called");
            }


        });

        function requestsRouter(rss_url) {

            let valid_url = /^https?:\/\/(www\.)?[-a-zA-Z0-9@:%_\+~#=]{2,256}\.[a-z]{2,6}.*/;
            let http = /^(http|https):\/\//;
            let rss_xml = /(rss\/?|\.xml)$/;
            let xml_extension = /\.xml$/;

            /* if empty url input field - empty livesearch live_search_drop_down */
            if (rss_url === "") {
                $(live_search_drop_down).empty();
                return;
            }

            /* if input has no http or https - auto complete. Add https:// */
            if (!http.test(rss_url)) {
                rss_url = "https:\/\/" + rss_url;
            }

            /* url validation */
            if (valid_url.test(rss_url)) {

                if (xml_extension.test(rss_url)) {
                    loadFeedsHTML(rss_url);
                    return;
                }
                if (!rss_xml.test(rss_url)) {
                    rss_url = rss_url + "\/rss";

                }
                console.log("OK - " + rss_url);
                console.log("waiting for respond");
                liveSearch(rss_url);

            } else {
                console.log(rss_url);
                $(live_search_drop_down).empty();
                console.log("Enter valid url");
            }

        }

        function liveSearch(rsssources) {
            $.ajax({
                method: "POST",
                url: "getfeedschannels",
                dataType: "html",
                data: {url: rsssources},
                headers:
                    {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                error: function (err) {
                    $(all_feeds).empty();
                    console.log("Call function liveSearch(). Error response: ");
                    console.log(err);
                },
                success: function (html) {
                    console.log("Function liveSearch() success!");
                    $(all_feeds).empty();

                    let xml_extension = /\.xml$/;

                    let link_check = $(html).find('a').first().attr('href');


                    if (!xml_extension.test(link_check)) {
                        console.log("Item.php links does not have .xml extension. Call loadFeedsHTML()");
                        loadFeedsHTML(rsssources, true);
                    } else {
                        console.log("Items links have extension .xml - " + link_check);

                        $(html).appendTo(live_search_drop_down);
                        $(live_search_drop_down).find("a").click(function (event) {
                            event.preventDefault();
                            $(live_search_input).val($(this).attr("href"));
                            $(live_search_drop_down).empty();
                            loadFeedsHTML($(this).attr("href"), true);
                        }); //click
                    }


                } //succes
            }); // ajax
        } // function liveSearch()

        /************************************    response in HTML format *****************************************/

        function loadFeedsHTML(source, showAllfeeds) {
            $.ajax({
                url: "getallfeeds",
                method: "POST",
                data: {url: source, showAllFeeds: showAllfeeds},
                dataType: 'html',
                headers:
                    {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                error: function (err) {
                    console.log("Call function loadFeedsHTML() cannot find xml document");
                    console.log(err);
                    $(live_search_drop_down).empty();
                    $(all_feeds).empty();
                },
                success: function (feed) {
                    $(all_feeds).empty();
                    $(live_search_drop_down).empty();
                    console.log("Function loadFeedsHTML() success!");
                    if ($(live_search_input) === "") {
                        $(all_feeds).empty();
                        return;
                    }
                    $(feed).appendTo(all_feeds);

                    initializePopoverForFollowBtn();

                    initializeModalsForItems();

                } //success
            }); //ajax
        } //loadFeedsHTML

        function get_hostname(url) {
            let m = url.match(/^(https?:\/\/)([^/]+)/);
            return m ? m[2] : null;
        }

        function initializeModalsForItems() {

            $(".feed-item").each(function () {
                let item = $(this);

                $(this).find(".feed-link").unbind("click").click(function (event) {

                    event.preventDefault();

                    let url = $(this).attr('href');
                    let title = $(this).text();
                    let date = $(item).find(".feed-date").text();
                    let img = $(item).find("input").val();
                    let description = $(item).find(".feed-description").text();
                    console.log(description);

                    let modal = $('#feedsModal');
                    modal.modal('show');
                    modal.find('.modal-header .modal-title').text(title);
                    modal.find('.modal-date').text(date);
                    modal.find('.modal-host').text(get_hostname(url));
                    console.log('img source: ' + img);
                    if (img === undefined || img === "") {
                        img = $(item).find("img").attr('src');

                    }
                    if (img === undefined || img === "") {
                        modal.find('.modal-body .modal-img').addClass('invisible');
                    } else {
                        modal.find('.modal-body .modal-img').removeClass('invisible').attr("src", img);
                    }

                    if (description !== '') {
                        modal.find('.modal-body').removeClass('d-none');
                        modal.find('.modal-body .card-text').text(description);
                    } else {
                        modal.find('.modal-body').addClass('d-none');
                    }
                    modal.find('.modal-body .card-text').text(description);
                    modal.find('.modal-footer .website-link').attr("href", url);

                });

            }); // each .feed-item prepare modal
        } // initializeModalsForItems

        function initializePopoverForUnfollow() {
            $('.btn-unfollow-feed').each(function () {
                $(this).popover({
                    container: 'body',
                    html: true,
                    template: '<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-header"></h3><div class="popover-body"></div></div>',
                    title: `Are you sure you want to remove feed - <strong>${$(this).closest('.card-body').find(".card-title").text()}</strong>?`,
                    content: function () {
                        return `<div class="form-group">
        <form action="/admin/unfollow-channel" method="post" class="d-flex justify-content-around align-items-center">
            <input type="hidden" name="delete_feed_id" value="${$(this).val()}">
            <input type="hidden" name="_token" value="${$('meta[name="csrf-token"]').attr('content')}">
            <button type="submit" class="btn btn-outline-danger col-6 mx-1">Yes</button>
            <button type="button" class="btn btn-outline-secondary col-6 mx-1">No</button>
        </form>
    </div>`;
                    },
                });
            });

        } // initializePopoverForFollowBtn

        function initializePopoverForFollowBtn() {
            $('.btn-follow-feed').popover({
                container: 'body',
                html: true,
                template: '<div class="popover" role="tooltip"><div class="arrow"></div><div class="popover-body p-0"></div></div>',
                content: function () {
                    return $('#follow_channel_popover_template').html();
                },
            });
            // });

        } // initializePopoverForFollowBtn


        initializePopoverForUnfollow();
        initializeModalsForItems();
    });
})(jQuery);