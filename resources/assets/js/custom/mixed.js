(function ($) {
    $(function () {
        let collapse_feeds = $("#sidebar_categories_and_feeds .collapse");

        $("#sidebar_categories_and_feeds .category").click(function (e) {
            e.preventDefault();
            $(this).closest('li').find('.collapse').collapse('toggle');
        });

        $(collapse_feeds).on('show.bs.collapse', function () {
            let down = '/images/feather/chevron-down.svg';
            $(this).closest('li').find('img').attr("src", down);
        });

        $(collapse_feeds).on('hide.bs.collapse', function () {
            let right = '/images/feather/chevron-right.svg';
            $(this).closest('li').find('img').attr("src", right);
        });

        $('#flash_message, #alert_message').delay(3000).fadeOut("slow");

    });
})(jQuery);