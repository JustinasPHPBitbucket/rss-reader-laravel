@if ($errors->any())
    <div class="offset-sm-3 col-sm-9 offset-lg-2 col-lg-10 d-flex flex-column py-1 mt-2">
    <div class="alert alert-danger alert-dismissible fade show" id="alert_message">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    </div>
@endif
<div class="offset-sm-3 col-sm-9 offset-lg-2 col-lg-10 d-flex flex-column">
    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))

                <div class="alert alert-{{ $msg }} alert-dismissible fade show" role="alert" id="flash_message">
                    {!! Session::get('alert-' . $msg) !!}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
        @endforeach
    </div> <!-- end .flash-message -->
</div>