<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1,user-scalable=yes">
<meta name="csrf-token" content="{{ csrf_token() }}">
<title>@yield('title')</title>
<meta name="description" content="Rss Reader - organize your favorite news"/>
<link href="{{asset('/css/app.css')}}" rel="stylesheet" type="text/css">
<link rel="icon" type="image/png" href="{{asset('/images//rss.png')}}">
