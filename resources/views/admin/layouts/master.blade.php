<!doctype html>
<html lang="en">
<head>
    @include('admin.layouts.head')
</head>
<body>
@include('admin.layouts.navbar')

<div class="container-fluid content">
    <div class="row">
        @include('admin.layouts.sidebar')
        @include('admin.layouts.alert_and_flash_messages')
        @yield('content')

    </div><!-- ./row -->
</div>

@include('admin.layouts.footer')
<!-- modals -->
@include('admin.modals.feed_modal')
@include("admin.modals.categories_modal")

<!-- popovers -->
@include('admin.popovers.follow_popover')


<!-- scripts -->
@include('admin.layouts.scripts')
</body>


</html>