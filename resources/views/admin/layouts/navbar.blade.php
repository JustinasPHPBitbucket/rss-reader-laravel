<nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">
    <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="/admin">Rss Reader</a>
    <a class="navbar-brand col-sm-2 col-md-1 ml-auto" href="/">Front page</a>
    <ul class="navbar-nav px-5">
        <li class="nav-item text-nowrap text-white">{{ Auth::user()->name }}</li>
    </ul>
    <ul class="navbar-nav px-5">
        <li class="nav-item text-nowrap">
            <a class="nav-link" href="{{ route('logout') }}"
               onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                {{ __('Logout') }}
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        </li>
    </ul>
</nav>