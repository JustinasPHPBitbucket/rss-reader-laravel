<nav class="col-sm-3 col-lg-2 d-sm-block bg-light sidebar">
    <div class="sidebar-sticky">
        <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 pt-5 pt-sm-2 text-muted">
            <a class="d-flex align-items-center pt-5 pt-sm-1 text-muted" href="/admin/addcontent">
                <span class="mr-2">Add NEW FEED</span>
                <span data-feather="plus-circle"></span>
            </a>
        </h6>

        <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 pt-2 text-muted">
            <a class="d-flex align-items-center  text-muted" data-toggle="modal" href="#categoriesModal">
                <span class="mr-2">EDIT CATEGORIES</span>
                <span data-feather="plus-circle"></span>
            </a>
        </h6>
        <hr>
        <ul class="nav flex-column">
            <li class="nav-item">
                <a class="nav-link" href="/admin">
                    <span data-feather="rss"></span> All Feeds
                </a>
            </li>
        </ul>
        <ul class="nav flex-column  pl-3 pb-5" id="sidebar_categories_and_feeds">
            @if (isset($categories))
                @foreach($categories as $category)
                    <li class="nav-item">
                        <div class="d-flex">
                            <a class="pr-2 nav-link category" href="#">
                                <img class="feather-icons" src="/images/feather/chevron-right.svg" alt="All feeds">
                            </a><a class="pl-0 nav-link d-flex col align-items-center justify-content-between"
                                   href="/admin/categories/{{$category->id}}"><span
                                        title="Filter feeds by category - {{$category->category_name}}">{{$category->category_name}}</span>
                                <span class="ml-auto badge badge-secondary badge-pill"
                                      title="{{count($category->channels)}} {{count($category->channels)== 1? 'feed':'feeds'}} in category {{$category->category_name}}">{{count($category->channels)}}</span>
                            </a>
                        </div>


                        @if (count($category->channels))
                            <ul class="collapse list-unstyled pl-4">
                                @foreach($category->channels as $channel)
                                    <li class="nav-item">
                                        <a class="nav-link" href="/admin/feeds/{{$channel->id}}" target="_blank">
                                            {{$channel->channel_title}}
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        @endif
                    </li>
                @endforeach
            @endif
        </ul>
    </div>
</nav>

