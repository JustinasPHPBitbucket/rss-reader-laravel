<div class="modal fade" id="categoriesModal" tabindex="-1" role="dialog" aria-hidden="true">
    <form method="post" id="new_category_form" action="/admin/create-new-category">
        @csrf
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="categoriesModalLabel">Categories</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    @if(count($categories))

                        <div style="position: relative;" class="d-block" id="selectCategoryDiv">
                            <p class="pb-1 mb-0">Edit category:</p>
                            <input type="text" class="form-control bg-white" placeholder="Select category..."
                                   id="selectCategoryInput" readonly="true">

                            <div class="invisible list-group col mt-0 p-0"
                                 style="position: absolute; z-index: 1000; overflow-y: scroll; max-height: 50vh"
                                 id="edit_category_drop_down">
                                @foreach($categories as $category)
                                    <div class="py-2 d-flex justify-content-between align-items-center list-group-item  list-group-item-action">
                                        <input type="hidden" value="{{$category->id}}"><span
                                                class="cat_title">{{$category->category_name}}</span>
                                        <span class="badge badge-info badge-pill">{{count($category->channels)}}</span>
                                    </div>
                                @endforeach
                            </div>

                        </div>
                        <div class="pt-2 form-group d-flex justify-content-end">
                            <button type="button" data-toggle="collapse" href="#collapse_new_category"
                                    class="btn btn-sm  btn-outline-success mr-auto">New
                            </button>
                            <button type="button" class="btn btn-sm btn-outline-warning disabled" id="cat_rename">
                                Rename
                            </button>
                            <button type="button" class="btn btn-sm btn-outline-success d-none" id="cat_rename_save">
                                Save
                            </button>
                            <button type="button" class="d-none ml-2 btn btn-sm btn-outline-warning" id="cat_cancel">
                                Cancel
                            </button>
                            <button type="button" class="btn btn-sm  btn-outline-danger ml-2 d-none" id="cat_delete">
                                Delete
                            </button>
                        </div>
                    @else
                        <div class="pt-2 form-group d-flex justify-content-end">
                            <button type="button" data-toggle="collapse" href="#collapse_new_category"
                                    class="btn btn-sm  btn-outline-success mr-auto">New
                            </button>
                        </div>
                    @endif

                </div><!-- ./modal-body -->
                <div class="collapse" id="collapse_new_category">
                    <div class="modal-footer d-block">
                        <div class="form-group">
                            <label for="category-name" class="col-form-label">Create new category:</label>
                            <input type="text" class="form-control" id="category-name" name="category_name"
                                   placeholder="Enter category name">
                            <input type="hidden" value="{{$user_id or "undefined"}}" name="user_id">
                        </div>
                        <div class="form-group d-flex justify-content-end">
                            <button type="submit" class="btn btn-success" id="new_category_sumbit_btn">Submit</button>
                            <button type="button" class="btn btn-secondary ml-2" data-dismiss="modal">Close</button>
                        </div>
                    </div><!-- ./modal-footer -->
                </div>

            </div>
        </div>
    </form>
</div>