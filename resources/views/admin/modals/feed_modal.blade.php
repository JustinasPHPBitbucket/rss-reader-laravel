<div class="modal fade" id="feedsModal"   role="dialog"  aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header flex-column ">

                    <div class="w-100 d-flex justify-content-between">
                        <h4 class="modal-title" id="feedsModalLabel">New message</h4>
                        <button type="button" class="ml-auto close align-self-center" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="w-100 d-flex justify-content-between">
                        <p class="text-muted small modal-date"></p>
                        <p class="text-muted small modal-host"></p>
                    </div>

            </div>

            <div class="modal-body pb-0">
                <div class="card border-0 mb-3">
                    <img class="card-img-top modal-img" src="#" alt="Card image cap">
                    <div class="card-body pt-0">
                        <h5 class="card-title"></h5>
                        <p class="card-text modal-description"></p>

                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <a href="#"  class="btn btn-primary website-link" target="_blank">VISIT WEBSITE</a>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>


</div>