@extends('admin.layouts.master')
@section('title', 'Add source | Rss Reader')
@section('content')
    <div class="offset-sm-3 col-sm-9 offset-lg-2 alert-row pt-5">

    </div>
    <div class="offset-sm-3 col-sm-9 offset-lg-2 d-flex flow-column flex-wrap justify-content-center">

        <div class="row">
            <div class="pt-5">
                <input class="form-control" type="text" name="url" size="100" placeholder="Enter url" id="live_search_url_input">
                <div class="" style="position: relative;" id="live_search_drop_down">
                </div>
            </div>

        </div><!-- ./row -->
        <div class="row align-content-stretch">
            <div class="col-12" id="all_feeds">
                {!! $feeds or "" !!}
            </div>
        </div><!-- ./row -->
    </div>
@endsection

