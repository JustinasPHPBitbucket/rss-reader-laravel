@extends('admin.layouts.master')
@section('title', $heading . ' | Rss Reader')
@section('content')
    <div class="offset-sm-3 col-sm-9 offset-lg-2 col-lg-10 d-flex flex-column">
        <h1 class="text-center py-5">{{$heading}}</h1>

        @foreach($feeds as $feed)

         @include('admin.templates.feeds.channelandfeeds')

        @endforeach
    </div>

@endsection
