@extends('admin.layouts.master')
@section('title', $feed->channel_title. ' | Rss Reader')
@section('content')
    <div class="offset-sm-3 col-sm-9 offset-lg-2 col-lg-10 d-flex flex-column">
        @include('admin.templates.feeds.channelandfeeds')

    </div>

@endsection
