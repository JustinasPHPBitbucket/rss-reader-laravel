<div id="follow_channel_popover_template" class="d-none p-0">

    @if (isset($categories))
        <ul class="list-group list-group-flush">
            @foreach($categories as $category)
                <li class=" list-group-item list-group-item-action d-flex align-items-center justify-content-between">
                    <span class="">{{$category->category_name}}</span>
                    <button onclick="$('.form-save-channel').find('input[name=category_id]').val('{{$category->id}}');
                            $('.form-save-channel').submit();" class="follow-btn btn btn-sm btn-outline-success ml-5"
                            title="Add to category - {{$category->category_name}}"><span data-feather="plus"></span> ADD
                    </button>
                </li>
            @endforeach
        </ul>
    @endif

</div>