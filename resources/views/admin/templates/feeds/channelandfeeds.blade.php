<div class="row pt-5">
    <div class="col-lg-8 offset-lg-2">
        <div class="d-flex pb-3 justify-content-start">
            <div class="card-body">
                <form action="/admin/follow-channel" method="post" class="hidden form-save-channel">
                    @csrf
                    <input type="hidden" name="url" value="{{$feed->channel_xml_url}}"/>
                    <input type="hidden" name="category_id" value="null"/>
                </form>
                <div class="d-flex justify-content-between flow-column flow-md-row flex-wrap">
                    <h2 class="card-title mb-1">{{$feed->channel_title}}</h2>
                    @if(isset($feed->category))
                        <div class="d-flex align-items-center">
                            @if(isset($feeds))
                                <a href="/admin/feeds/{{$feed->id}}" class="btn btn-outline-success btn-sm"
                                   target="_blank">Read feed
                                </a>
                            @endif
                            <button type="button" name="delete_feed_id"
                                    class="btn btn-outline-danger btn-sm ml-2 btn-unfollow-feed" value="{{$feed->id}}">
                                Unfollow
                            </button>
                        </div><!-- ./unfollow -->
                    @else
                        <div class="d-flex justify-content-between flow-column flow-md-row flex-wrap">
                            <div class="d-flex align-items-center">
                                <button class="btn btn-outline-success btn-sm btn-follow-feed">Follow</button>
                            </div>
                        </div>
                    @endif
                </div>
                <p class="small mt-0 ">{{$feed->channel_link}}</p>
                <p>
                </p>
                <p class="cart-text ">{{$feed->channel_description}}</p>
                @foreach ($feed->items as $key => $item)
                    @if($key < 5)
                        <p class="small my-0 py-0">{{$item->item_title }}</p>
                    @endif
                @endforeach
                @if(isset($feed->category))
                    <p class="pt-2">Category: {{$feed->category->category_name}}</p>
                @endif
            </div><!-- ./card-body -->
        </div>
    </div><!-- ./col-8 -->
</div><!-- ./row -->

@if(! isset($feeds))
    @foreach ($feed->items as $key => $item)
        <div class="row">
            <div class="col-lg-8 offset-lg-2">
                <div class="feed-item d-flex border-bottom pb-3 pt-3  flex-column flex-sm-row">
                    @if ($item->item_mediaThumbnail !== null)
                        <div class="d-flex justify-content-center align-items-start">
                            <img class="p-2 feed-img" style="border-radius: 20px" width="120"
                                 src="{{$item->item_mediaThumbnail}}">
                            <input type="hidden" value="{{$item->item_mediaContent}}">
                        </div>
                    @endif
                    <div class="pt-2 pl-2 align-items-start">
                        <h5 class="card-title"><a class="feed-link"
                                                  href="{{$item->item_link}}"
                                                  target="_blank">{{$item->item_title}}</a>
                        </h5>
                        <p class="small text-muted feed-date">{{date("Y-m-d h:i", strtotime($item->item_pubDate))}}</p>
                        @if ($item->item_description !== 0)
                            <p class="card-text feed-description">{!! $item->item_description !!}</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endif

