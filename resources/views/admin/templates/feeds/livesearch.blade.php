@if (isset($items))
    <div class="mb-5 px-0 col-12 position-absolute"  class="list-group" id="rss_sources">
        @foreach($items as $item)
            <a href="{{$item->getElementsByTagName('link')->item(0)->childNodes->item(0)->nodeValue}}"
               class="list-group-item list-group-item-action">{{$item->getElementsByTagName('title')->item(0)->childNodes->item(0)->nodeValue}}</a>
        @endforeach
    </div>
@endif

