@extends('front.layouts.master')
@section('content')
<div class="row">
    <div class="col-sm-12 d-flex justify-content-center">
        <div class="jumbotron text-center align-self-lg-start">
        <h1>Welcome to RSS Reader</h1>
            <p class="lead">Organize your sources into easy-to-read feeds.</p>
            @auth
                <a  class="btn btn-success" href="/admin">Back to admin panel</a>
                @else
                    <a  class="btn btn-success" href="/login">Login to get started</a>
                    @endauth
        </div> <!-- ./jumbotron -->
        <div class="col-sm-8">
        <img class="w-100 m-auto" src="{{asset('/images/front-page-feeds.jpg')}}" alt="Feeds">
        </div>
    </div>
</div>
@endsection
