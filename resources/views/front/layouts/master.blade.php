<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    @include('front.layouts.head')
</head>
<body>
@include('front.layouts.navbar')

<div class="container mt-5">
    @yield('content')
</div>

@include('front.layouts.footer')
@include('front.layouts.scripts')
</body>
</html>
