<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container">
    <a class="navbar-brand px-2 bg-transparent" style="box-shadow: none" href="/">RSS Reader</a>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">

            @if (Route::has('login'))
            <ul class="navbar-nav ml-auto">
                    @auth
                    <li class="nav-item"><a  class="nav-link" href="{{ url('/admin') }}">Back to admin panel</a></li>
                        @else
                        <li class="nav-item"><a class="nav-link" href="{{ route('login') }}">Login</a></li>
                            @endauth
            </ul>
            @endif
    </div>
    </div>
</nav>