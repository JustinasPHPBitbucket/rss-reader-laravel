<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('front.index');
});

Auth::routes();


Route::middleware(['auth'])->group(function () {
    Route::prefix('admin')->group(function () {
        Route::get('/', 'PagesController@index')->name('admin');
        Route::get('/categories/{id}', 'PagesController@filterByCategories');
        Route::get('/addcontent', 'PagesController@addContent');
        Route::get('/feeds/{id}', 'PagesController@currentFeed');

        Route::post('/getfeedschannels', "PagesController@liveSearch");
        Route::post('/getallfeeds', "PagesController@getChannelTitleAndAllItems");
        Route::post('/create-new-category', "CategoriesController@store");
        Route::post('/delete-category', "CategoriesController@destroy");
        Route::post('/rename-category', "CategoriesController@update");
        Route::post('/follow-channel', "FeedsController@store");
        Route::post('/unfollow-channel', "FeedsController@deleteFeed");
    });
});





